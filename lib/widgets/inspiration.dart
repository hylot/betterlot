import 'package:better_LOT/model/inspiration.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class InspirationWidget extends StatelessWidget {
  final LinearGradient theme;
  final inspiration inspirationaliment;
  final VoidCallback increment;
  final VoidCallback decrement;

  InspirationWidget({
    @required this.inspirationaliment,
    @required this.theme,
    this.increment,
    this.decrement
  });

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          SvgPicture.asset(
            inspirationaliment.image,
            width: 70.0,
            height: 70.0,
          ),
          Container(
            child: Column(
              children: <Widget>[
                Text(inspirationaliment.name,
                    style: TextStyle(
                        fontSize: 40.0,
                        fontWeight: FontWeight.w300,
                        fontFamily: 'Roboto')),
                Padding(
                  padding: EdgeInsets.only(top: 15.0),
                  child: Text("• " + inspirationaliment.subtitle + " •",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 17.0,
                        fontFamily: 'Dosis',
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ],
            ),
          ),
          Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      color: theme.colors[0]
                  ),
                  width: 70,
                  height: 1.0,
                ),
                Container(
                  child: OutlineButton(
                    borderSide: BorderSide(color: theme.colors[0]),
                    onPressed: () => null,
                    shape: StadiumBorder(),
                    child: SizedBox(
                      width: 60.0,
                      height: 45.0,
                      child: Center(
                          child: Text(
                              '${inspirationaliment.price.toInt()}' + " EUR",
                              style: TextStyle(
                                  color: theme.colors[0],
                                  fontSize: 17.0,
                                  fontWeight: FontWeight.w400
                              ),
                              textAlign: TextAlign.center)),
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      color: theme.colors[0]
                  ),
                  width: 70,
                  height: 1.0,
                ),
              ]),
        ]);
  }
}