import 'package:better_LOT/model/mainmenu.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../main.dart';

class MainMenuWidget extends StatelessWidget {
  final LinearGradient theme;
  final mainmenu mainmenualiment;
  final VoidCallback increment;
  final VoidCallback decrement;

  MainMenuWidget(
      {@required this.mainmenualiment,
        @required this.theme,
        this.increment,
        this.decrement});

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          SvgPicture.asset(
            mainmenualiment.image,
            width: 90.0,
            height: 90.0,
          ),
          Container(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 5.0,left: 20.0,right: 20.0),
                  child:
                  new MaterialButton(
                    height: 70.0,
                    minWidth: double.infinity,
                    color: const Color(0xfE0b0866),
                    textColor: Colors.white,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16.0))),
                    child: new Text("Daily Inspirations", style: new TextStyle(
                      fontSize: 18.0,)),
                    onPressed:() {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => InspirationSettingsPage())
                      );
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 5.0,left: 20.0,right: 20.0),
                  child:
                  new MaterialButton(
                    height: 70.0,
                    minWidth: double.infinity,
                    color: const Color(0xFE053380),
                    textColor: Colors.white,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16.0))),
                    child: new Text("Find flight", style: new TextStyle(
                      fontSize: 20.0,)),
                    onPressed:() {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => InspirationPage())
                      );
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 5.0,left: 20.0,right: 20.0),
                  child:
                  new MaterialButton(
                    height: 70.0,
                    minWidth: double.infinity,
                    color: const Color(0xFF2c58a3),
                    textColor: Colors.white,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16.0))),
                    child: new Text("My tickets", style: new TextStyle(
                      fontSize: 20.0,)),
                    onPressed:() {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => TicketPage())
                      );
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 5.0,left: 20.0,right: 20.0),
                  child:
                  new MaterialButton(
                    height: 70.0,
                    minWidth: double.infinity,
                    color: const Color(0xFF366bc7),
                    textColor: Colors.white,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16.0))),
                    child: new Text("Log out", style: new TextStyle(
                      fontSize: 20.0,)),
                    onPressed:() {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => TicketPage())
                      );
                    },
                  ),
                )
              ],
            ),
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Container(
              decoration: BoxDecoration(color: theme.colors[0]),
              width: 90,
              height: 1.0,
            ),
            Container(
              decoration: BoxDecoration(color: theme.colors[0]),
              width: 90,
              height: 1.0,
            )
          ])
        ]);
  }
}
