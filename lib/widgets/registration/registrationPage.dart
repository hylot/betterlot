import 'package:better_LOT/model/registration.dart';
import 'package:better_LOT/widgets/card_item.dart';
import 'package:better_LOT/widgets/page.dart';
import 'package:better_LOT/widgets/pager.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'registrationWidget.dart';

class RegistrationPage extends StatelessWidget {
  RegistrationPage() {
    SystemChrome.setPreferredOrientations(
        <DeviceOrientation>[DeviceOrientation.portraitUp]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: MenuPager(
            children: [Page(
              title: "Rejestracja",
              child: CardItem(
                  child: RegistrationWidget(registration: Registration())))]),
      ),
    );
  }
}
