import 'package:better_LOT/data/airports.dart';
import 'package:better_LOT/model/registration.dart';
import 'package:flutter/material.dart';

class RegistrationWidget extends StatefulWidget {
  final Registration registration;

  RegistrationWidget({
    @required this.registration,
  });

  @override
  State<StatefulWidget> createState() {
    return _RegistrationWidgetState();
  }
}

class _RegistrationWidgetState extends State<RegistrationWidget> {
  int counter = 0;
  DateTime _date;

  Future<Null> _selectedDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _date,
        firstDate: new DateTime(2016),
        lastDate: new DateTime(2019));

    if (picked != null && picked != _date) {
      print("Date selected ${_date.toString()}");
      setState(() {
        _date = picked;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text("From"),
        DropdownButton<String>(
          value: Airports.first,
          items: Airports.map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
                value: value,
                child: Text(value)
            );
          }).toList(),
          onChanged: (newValue) {
            setState(() {
              widget.registration.departureAirport = newValue;
            });
          },
        ),
        Text("Departure date"),
        Row(
          children: <Widget>[
            Text("${widget.registration.departureDate.toString()}"),
            MaterialButton(child: Text("Select date"),
                onPressed: () async =>{ await _selectedDate(context)
//                    () => {
//                setState(() async {
////                  widget.registration.departureDate = DateTime.now();
//                  widget.registration.departureDate =
//                    await showDatePicker(context: context,
//                    initialDate: DateTime.now(),
//                    firstDate: DateTime.now(),
//                    lastDate: DateTime.now().add(Duration(days: 365)));
//                })
            }
            )
          ],
        ),
        TextField(onChanged: (text) => widget.registration.arrivalAirport),
        TextField(onChanged: (text) => widget.registration.departureAirport)
//
      ],
    );
  }

}