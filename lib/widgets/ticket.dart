import 'package:better_LOT/model/ticket.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class TicketWidget extends StatelessWidget {
  final LinearGradient theme;
  final ticket ticketaliment;
  final VoidCallback increment;
  final VoidCallback decrement;


  TicketWidget({
    @required this.ticketaliment,
    @required this.theme,
    this.increment,
    this.decrement
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Container(
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 0.0),
                child: Text(ticketaliment.name,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 16.0,
                      fontFamily: 'Dosis',
                      fontWeight: FontWeight.w400),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 0.0),
                child: Text(ticketaliment.from + " - " + ticketaliment.to,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 24.0,
                      fontFamily: 'Dosis',
                      fontWeight: FontWeight.w400),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15.0),
                child: Text(ticketaliment.when,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 27.0,
                      fontFamily: 'Dosis',
                      fontWeight: FontWeight.w400),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15.0),
                child: Text(ticketaliment.resnumber,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 17.0,
                      fontFamily: 'Dosis',
                      fontWeight: FontWeight.w400),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15.0),
                child: Text("Seat: 22A" ,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 17.0,
                      fontFamily: 'Dosis',
                      fontWeight: FontWeight.w400),
                ),
              ),
          Padding(
              padding: EdgeInsets.only(top: 35.0),
          ),
                  SvgPicture.asset(
                    ticketaliment.assetName,
                    width: 180.0,
                    height: 180.0,
                    ),
              Padding(
                padding: EdgeInsets.only(top: 15.0),
                child: Text(ticketaliment.from + " - " + ticketaliment.to,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 17.0,
                      fontFamily: 'Dosis',
                      fontWeight: FontWeight.w400),
                ),
              ),
            ],
          ),
        )
      ]
    );
  }
}

