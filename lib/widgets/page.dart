import 'package:flutter/material.dart';

class Page extends StatelessWidget {
  final LinearGradient background;
//  = const LinearGradient(
//      begin: Alignment.topRight,
//      end: Alignment.bottomLeft,
//      colors: [const Color(0xFF2c58a3), const Color(0xFF6EA3FF)],
//    );
  final String title;
  final String icon;
  final Widget child;

  const Page({
    @required this.title,
    this.background = const LinearGradient(
      begin: Alignment.topRight,
      end: Alignment.bottomLeft,
      colors: [const Color(0xFF2c58a3), const Color(0xFF6EA3FF)],
    ),
    @required this.icon,
    @required this.child
  });

  @override
  Widget build(BuildContext context) {
    return child;
  }
}
