import 'package:better_LOT/data/airports.dart';
import 'package:better_LOT/model/inspirationsetting.dart';
import 'package:better_LOT/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class InspirationSettingWidget extends StatefulWidget {
  final LinearGradient theme;
  final inspirationsetting inspirationsettingaliment;
  final VoidCallback increment;
  final VoidCallback decrement;

  InspirationSettingWidget(
      {@required this.inspirationsettingaliment,
      @required this.theme,
      this.increment,
      this.decrement});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _InspirationSettingWidgetState();
  }
}

class _InspirationSettingWidgetState extends State<InspirationSettingWidget> {
  String airport = Airports.first;
  String month;
  List<String> months = [
    "January  ",
    "February ",
    "March    ",
    "April    ",
    "May      ",
    "June     ",
    "July     ",
    "August   ",
    "September",
    "October  ",
    "November ",
    "December "
  ];

  _InspirationSettingWidgetState() {
    month = months[DateTime.now().month - 1];
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <
        Widget>[
      SvgPicture.asset(
        widget.inspirationsettingaliment.image,
        width: 190.0,
        height: 190.0,
      ),
      Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          DropdownButton<String>(
            value: airport,
            items: Airports.map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(value: value, child: Text(value));
            }).toList(),
            onChanged: (newValue) {
              setState(() {
                airport = newValue;
              });
            },
          ),
          DropdownButton<String>(
            value: month,
            items: months.map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(value: value, child: Text(value));
            }).toList(),
            onChanged: (newValue) {
              setState(() {
                month = newValue;
              });
            },
          ),
          Padding(
            padding: EdgeInsets.only(top: 5.0, left: 20.0, right: 20.0),
            child: new MaterialButton(
              height: 70.0,
              minWidth: double.infinity,
              color: const Color(0xfE0b0866),
              textColor: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(16.0))),
              child: new Text("Inspirations",
                  style: new TextStyle(
                    fontSize: 20.0,
                  )),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => InspirationPage()));
              },
            ),
          ),
        ],
      )
    ]);
  }
}
