import 'package:flutter/material.dart';

class inspiration {
  final String name;
  final LinearGradient background;
  final String subtitle;
  final String image;
  final double price;

  inspiration({ this.name,
    this.background,
    this.subtitle,
    this.image,
    this.price
  });

}