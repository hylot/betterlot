import 'package:better_LOT/model/ticket.dart';
import 'package:flutter/material.dart';


class Tickets {
  static List<ticket> tickets = [
    ticket(
        name: "LO3325 LOT Polish Airlines",
        from: "Bydgoszcz",
        to: "Warszawa",
        when: "2019-10-13 08:14",
        resnumber: "AZ9345987",
        background: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [const Color(0xFF2c58a3), const Color(0xFF6EA3FF)],
        ),
        seat: "22b",
        assetName: 'assets/images/qrcodebiletu.svg')
  ];
}