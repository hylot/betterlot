import 'package:better_LOT/model/mainmenu.dart';
import 'package:flutter/material.dart';

class MainMenus {
  static List<mainmenu> mainmenus = [
    mainmenu(
        background: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [const Color(0xFF2c58a3), const Color(0xFF6EA3FF)],
        ),
        image: "assets/images/LOT.svg"
    )
  ];
}