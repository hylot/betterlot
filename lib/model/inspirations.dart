import 'package:better_LOT/model/inspiration.dart';
import 'package:flutter/material.dart';

class Inspirations {
  static List<inspiration> inspirations = [
    inspiration(
        name: "Germany",
        background: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [const Color(0xFF2c58a3), const Color(0xFF6EA3FF)],
        ),
        subtitle: "Monachium",
        image: "assets/images/germany.svg",
        price: 120.0),
    inspiration(
        name: "Austria",
        background: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [const Color(0xFF2c58a3), const Color(0xFF6EA3FF)],
        ),
        subtitle: "Viena",
        image: "assets/images/austria.svg",
        price: 560.0),

    inspiration(
        name: "Polska",
        background: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [const Color(0xFF2c58a3), const Color(0xFF6EA3FF)],
        ),
        subtitle: "Kraków",
        image: "assets/images/poland.svg",
        price: 210.0),
    inspiration(
        name: "Germany",
        background: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [const Color(0xFF2c58a3), const Color(0xFF6EA3FF)],
        ),
        subtitle: "Frankfurt",
        image: "assets/images/germany.svg",
        price: 238.0),

    inspiration(
        name: "Italy",
        background: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [const Color(0xFF2c58a3), const Color(0xFF6EA3FF)],
        ),
        subtitle: "Rome",
        image: "assets/images/italy.svg",
        price: 195.0),

  ];
}
