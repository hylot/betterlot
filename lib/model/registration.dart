import 'package:better_LOT/model/cabinClass.dart';
import 'package:better_LOT/model/airport.dart';

class Registration {
  String departureAirport;
  String arrivalAirport;

  DateTime departureDate;
  DateTime arrivalDate;

  bool setArrival;

  CabinClass cabinClass;
  Registration();
}

