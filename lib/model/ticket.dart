import 'package:flutter/material.dart';

class ticket {
  final String name;
  final String from;
  final String to;
  final String when;
  final String resnumber;
  final String seat;
  final LinearGradient background;
  final String assetName;


  ticket({ this.name,
    this.from,
    this.to,
    this.when,
    this.resnumber,
    this.background,
    this.seat,
    this.assetName
  });

}