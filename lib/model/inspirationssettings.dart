import 'package:better_LOT/model/inspirationsetting.dart';
import 'package:flutter/material.dart';

class InspirationsSettings {
  static List<inspirationsetting> inspirationssettings = [
    inspirationsetting(
        from: "Dupa settings",
        when: "Listopad",
        background: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [const Color(0xFF2c58a3), const Color(0xFF6EA3FF)],
        ),
        image: "assets/images/plane.svg",
    )
  ];
}