
import 'package:better_LOT/model/inspirationsetting.dart';
import 'package:better_LOT/model/inspirations.dart';
import 'package:better_LOT/model/inspirationssettings.dart';
import 'package:better_LOT/model/tickets.dart';
import 'package:better_LOT/model/mainmenus.dart';

import 'package:better_LOT/widgets/mainmenu.dart';
import 'package:better_LOT/widgets/inspiration.dart';
import 'package:better_LOT/widgets/ticket.dart';
import 'package:better_LOT/widgets/inspirationSetting.dart';

import 'package:better_LOT/widgets/card_item.dart';
import 'package:better_LOT/widgets/page.dart';
import 'package:better_LOT/widgets/pager.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'BetterLOT',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MainMenu(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MainMenu extends StatelessWidget {
  MainMenu() {
    SystemChrome.setPreferredOrientations(
        <DeviceOrientation>[DeviceOrientation.portraitUp]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: MenuPager(
          children: MainMenus.mainmenus
              .map(
                (mainmenu) => Page(
                  title: "",
                  background: mainmenu.background,
                  child: CardItem(
                    child: MainMenuWidget(
                      mainmenualiment: mainmenu,
                      theme: mainmenu.background,
                    ),
                  ),
                ),
              )
              .toList(),
        ),
      ),
    );
  }
}

////

class InspirationSettingsPage extends StatelessWidget {
  InspirationSettingsPage() {
    SystemChrome.setPreferredOrientations(
        <DeviceOrientation>[DeviceOrientation.portraitUp]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: MenuPager(
          children: InspirationsSettings.inspirationssettings
              .map(
                (inspirationsetting) => Page(
              title: "",
              background: inspirationsetting.background,
              child: CardItem(
                child: InspirationSettingWidget(
                  inspirationsettingaliment :  inspirationsetting,
                  theme: inspirationsetting.background,
                ),
              ),
            ),
          )
              .toList(),
        ),
      ),
    );
  }
}


class InspirationPage extends StatelessWidget {
  InspirationPage() {
    SystemChrome.setPreferredOrientations(
        <DeviceOrientation>[DeviceOrientation.portraitUp]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: MenuPager(
          children: Inspirations.inspirations
              .map(
                (inspiration) => Page(
                  title: "",
                  background: inspiration.background,
                  child: CardItem(
                    child: InspirationWidget(
                      inspirationaliment: inspiration,
                      theme: inspiration.background,
                    ),
                  ),
                ),
              )
              .toList(),
        ),
      ),
    );
  }
}

// //

class TicketPage extends StatelessWidget {
  TicketPage() {
    SystemChrome.setPreferredOrientations(
        <DeviceOrientation>[DeviceOrientation.portraitUp]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: MenuPager(
          children: Tickets.tickets
              .map(
                (ticket) => Page(
                  title: "",
                  background: ticket.background,
                  child: CardItem(
                    child: TicketWidget(
                      ticketaliment: ticket,
                      theme: ticket.background,
                    ),
                  ),
                ),
              )
              .toList(),
        ),
      ),
    );
  }
}


